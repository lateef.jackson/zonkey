const std = @import("std");
const Token = @import("token.zig").Token;
const token = @import("token.zig");
const Allocator = std.mem.Allocator;

const AstError = error{
    Unknown,
    ExpectedInflixExpression,
    ExpectedPefixExpression,
};
pub const Identifier = struct {
    token: Token,
    value: []const u8,
    pub fn init(tok: Token, val: []const u8) Identifier {
        return Identifier{
            .token = tok,
            .value = val,
        };
    }
    pub fn literal(self: Identifier) []const u8 {
        return self.token.literal;
    }
    pub fn string(self: ReturnStatement, alloc: std.mem.Allocator, n: ?Node) ![]const u8 {
        _ = alloc;
        _ = n;
        return self.literal();
    }
};

pub const Integer = struct {
    token: Token,
    value: i64,
    pub fn init(tok: Token, val: i64) Integer {
        return Integer{
            .token = tok,
            .value = val,
        };
    }
    pub fn literal(self: Integer) []const u8 {
        return self.token.literal;
    }
    pub fn string(self: ReturnStatement, alloc: std.mem.Allocator, n: ?Node) ![]const u8 {
        _ = alloc;
        _ = n;
        return self.literal();
    }
};

pub const Boolean = struct {
    token: Token,
    value: bool,
    pub fn init(tok: Token, val: bool) Boolean {
        return Boolean{
            .token = tok,
            .value = val,
        };
    }
    pub fn literal(self: Boolean) []const u8 {
        return self.token.literal;
    }
    pub fn string(self: ReturnStatement, alloc: std.mem.Allocator, n: ?Node) ![]const u8 {
        _ = alloc;
        _ = n;
        return self.literal();
    }
};

pub const String = struct {
    token: Token,
    value: []const u8,
    pub fn init(tok: Token, val: []const u8) String {
        return String{
            .token = tok,
            .value = val,
        };
    }
    pub fn literal(self: String) []const u8 {
        return self.token.literal;
    }
    pub fn string(self: ReturnStatement, alloc: std.mem.Allocator, n: ?Node) ![]const u8 {
        _ = n;
        _ = alloc;
        return self.literal();
    }
};

pub const PrefixExpression = struct {
    token: Token,
    operator: []const u8,
    pub fn init(tok: Token, op: []const u8) PrefixExpression {
        return PrefixExpression{
            .token = tok,
            .operator = op,
        };
    }
    pub fn literal(self: PrefixExpression) []const u8 {
        return self.token.literal;
    }

    pub fn string(self: ReturnStatement, alloc: std.mem.Allocator, n: ?Node) ![]const u8 {
        if (n != null and n.children.len == 1) {
            return AstError.ExpectedPefixExpression;
        }
        const right = n.children[0];
        var out = try std.Buffer.init(alloc, self.token.literal);
        try out.append("(");
        try out.append(self.operator);
        try out.append(right.string());
        try out.append(")");
        if (n) |node| {
            try out.append(node.string(alloc, n));
        }
        try out.append(";");
        return out.toSlice();
    }
};

pub const InfixExpression = struct {
    token: Token,
    operator: []const u8,
    pub fn init(tok: Token, op: []const u8) InfixExpression {
        return PrefixExpression{
            .token = tok,
            .operator = op,
        };
    }
    pub fn literal(self: InfixExpression) []const u8 {
        return self.token.literal;
    }

    pub fn string(self: ReturnStatement, alloc: std.mem.Allocator, n: ?Node) ![]const u8 {
        if (n != null and n.children.len < 2) {
            return AstError.ExpectedInflixExpression;
        }
        const left = n.children[0];
        const right = n.children[1];
        var out = try std.Buffer.init(alloc, self.token.literal);

        try out.append("(");
        try out.append(left.string());
        try out.append(self.operator);
        try out.append(right.string());
        try out.append(")");
        if (n) |node| {
            try out.append(node.string(alloc, n));
        }
        try out.append(";");
        return out.toSlice();
    }
};

pub const LetStatement = struct {
    token: Token,
    name: Identifier,
    pub fn init(tok: Token, idtf: Identifier) LetStatement {
        return LetStatement{
            .token = tok,
            .name = idtf,
        };
    }
    pub fn literal(self: LetStatement) []const u8 {
        return self.token.literal;
    }
    pub fn string(self: ReturnStatement, alloc: std.mem.Allocator, n: ?Node) ![]const u8 {
        if (n != null and n.children.len == 1) {
            return AstError.ExpectedPefixExpression;
        }
        const right = n.children[0];
        var out = try std.Buffer.init(alloc, self.token.literal);
        try out.append(self.name.value);
        try out.append(" = ");
        try out.append(right.string(alloc, n));
        try out.append(";");
        return out.toSlice();
    }
};

pub const ReturnStatement = struct {
    token: Token,
    pub fn init(tok: Token) ReturnStatement {
        return ReturnStatement{
            .token = tok,
        };
    }

    pub fn literal(self: ReturnStatement) []const u8 {
        return self.token.literal;
    }
    pub fn string(self: ReturnStatement, alloc: std.mem.Allocator, n: ?Node) ![]const u8 {
        var out = try std.Buffer.init(alloc, self.token.literal);
        try out.append("return");
        try out.append(" ");
        if (n) |node| {
            try out.append(node.string(alloc, n));
        }
        try out.append(";");
        return out.toSlice();
    }
};

pub const ExpressionStatement = struct {
    token: Token,
    pub fn init(tok: Token) ExpressionStatement {
        return ExpressionStatement{
            .token = tok,
        };
    }
    pub fn literal(self: ExpressionStatement) []const u8 {
        return self.token.literal;
    }
    pub fn string(self: ExpressionStatement, alloc: std.mem.Allocator, n: ?Node) ![]const u8 {
        var out = try std.Buffer.init(alloc, self.token.literal);
        try out.append("return");
        try out.append(" ");
        if (n) |node| {
            try out.append(node.string(alloc, n));
        }
        try out.append(";");
        return out.toSlice();
    }
};

pub const Expression = union(enum) {
    infix: InfixExpression,
    prefix: PrefixExpression,
    pub fn node(self: Statement) Node {
        switch (self) {
            inline else => |case| return case,
        }
    }
    pub fn statementNode(self: Statement) void {
        switch (self) {
            inline else => |case| return case.statementNode(),
        }
    }
    pub fn literal(self: Statement) []u8 {
        return self.node().literal();
    }
    pub fn string(self: Statement) []u8 {
        return self.node().string();
    }
};

pub const Statement = union(enum) {
    let: LetStatement,
    ret: ReturnStatement,
    exp: ExpressionStatement,
    pub fn node(self: Statement) Node {
        switch (self) {
            inline else => |case| return case,
        }
    }
    pub fn statementNode(self: Statement) void {
        switch (self) {
            inline else => |case| return case.statementNode(),
        }
    }
    pub fn literal(self: Statement) []u8 {
        return self.node().literal();
    }
    pub fn string(self: Statement, alloc: std.mem.Allocator, n: Node) []u8 {
        return self.node().string(alloc, n);
    }
};

// Node is a tree data structure to represent the AST
pub const Node = struct {
    pub const Type = union(enum) {
        identifier: Identifier,
        integer: Integer,
        boolean: Boolean,
        string: String,
        expression: Expression,
        statement: Statement,
    };

    alloc: std.mem.Allocator,
    data: Type,
    children: ?*std.ArrayList(Node),
    parent: ?*Node, // This is the Program
    pub fn init(alloc: std.mem.Allocator, data: Type, parent: ?*Node, children: ?*std.ArrayList(Node)) Node {
        return Node{
            .alloc = alloc,
            .parent = parent,
            .data = data,
            .children = children,
        };
    }
    pub fn string(self: Node) ![]u8 {
        var out = std.ArrayList([]u8).init(self.alloc);

        if (self.children) |children| {
            for (children.items) |c| {
                switch (c.data) {
                    Node.Type.statement => {
                        try out.append(c.data.statement.string(self.alloc, self));
                    },
                    else => {},
                }
            }
        }
        return out.toSlice();
    }
};

pub const Program = struct {
    root: Node,
};

const test_allocator = std.testing.allocator;

test "test simple node string" {
    const n = Node.init(
        test_allocator,
        Node.Type{ .integer = Integer.init(Token{ .literal = "5", .token = token.Type.INT, .line = 1 }, 5) },
        null,
        null,
    );

    try std.testing.expect(std.mem.eql(u8, try n.string(), "5"));
}
