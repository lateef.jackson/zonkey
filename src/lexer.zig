const std = @import("std");
const token = @import("./token.zig");

fn concat(allocator: std.mem.Allocator, a: []const u8, b: []const u8) ![]u8 {
    const result = try allocator.alloc(u8, a.len + b.len);
    std.mem.copy(u8, result, a);
    std.mem.copy(u8, result[a.len..], b);
    return result;
}

fn isDigit(c: u8) bool {
    return switch (c) {
        '0'...'9' => true,
        else => false,
    };
}

fn isLetter(c: u8) bool {
    return switch (c) {
        'a'...'z', 'A'...'Z', '_' => true,
        else => false,
    };
}
pub const Lexer = struct {
    const Self = @This();
    input: []const u8,
    position: usize,
    readPosition: usize,
    ch: u8,
    line: i32,
    pub fn init(input: []const u8) Lexer {
        var l = Lexer{
            .input = input,
            .position = 0,
            .readPosition = 0,
            .ch = 0,
            .line = 1,
        };

        l.readChar();
        return l;
    }

    pub fn readChar(self: *Self) void {
        if (self.readPosition >= self.input.len) {
            self.ch = 0;
        } else {
            self.ch = self.input[self.readPosition];
        }
        self.position = self.readPosition;
        self.readPosition += 1;
    }

    fn peekChar(self: *Self) u8 {
        if (self.readPosition >= self.input.len) {
            return 0;
        } else {
            return self.input[self.readPosition];
        }
    }

    pub fn nextToken(self: *Self) token.Token {
        self.skipWhitespace();
        defer self.readChar();
        switch (self.ch) {
            '=' => {
                if (self.peekChar() == '=') {
                    self.readChar();
                    return self.makeToken(token.Type.EQ, self.position - 1, self.readPosition);
                } else {
                    return self.makeToken(token.Type.ASSIGN, self.position, self.readPosition);
                }
            },
            '!' => {
                if (self.peekChar() == '=') {
                    self.readChar();
                    return self.makeToken(token.Type.NOT_EQ, self.position - 1, self.readPosition);
                } else {
                    return self.makeToken(token.Type.BANG, self.position, self.readPosition);
                }
            },
            ';' => return self.makeToken(token.Type.SEMICOLON, self.position, self.readPosition),
            ':' => return self.makeToken(token.Type.COLON, self.position, self.readPosition),
            '(' => return self.makeToken(token.Type.LPAREN, self.position, self.readPosition),
            ')' => return self.makeToken(token.Type.RPAREN, self.position, self.readPosition),
            ',' => return self.makeToken(token.Type.COMMA, self.position, self.readPosition),
            '+' => return self.makeToken(token.Type.PLUS, self.position, self.readPosition),
            '-' => return self.makeToken(token.Type.MINUS, self.position, self.readPosition),
            '/' => return self.makeToken(token.Type.SLASH, self.position, self.readPosition),

            '*' => return self.makeToken(token.Type.ASTERISK, self.position, self.readPosition),
            '<' => return self.makeToken(token.Type.LT, self.position, self.readPosition),
            '>' => return self.makeToken(token.Type.GT, self.position, self.readPosition),
            '{' => return self.makeToken(token.Type.LBRACE, self.position, self.readPosition),
            '}' => return self.makeToken(token.Type.RBRACE, self.position, self.readPosition),
            '[' => return self.makeToken(token.Type.LBRACKET, self.position, self.readPosition),
            ']' => return self.makeToken(token.Type.RBRACKET, self.position, self.readPosition),
            '"' => return self.readString(),
            0 => return token.Token{ .line = self.line, .token = token.Type.EOF, .literal = "" },
            else => {
                if (isLetter(self.ch)) {
                    return self.readIdentifier();
                } else if (isDigit(self.ch)) {
                    return self.readInt();
                } else {
                    return self.makeToken(token.Type.ILLEGAL, self.position, self.readPosition);
                }
            },
        }
    }

    fn makeToken(self: *Self, tt: token.Type, start: usize, end: usize) token.Token {
        return token.Token{
            .line = self.line,
            .token = tt,
            .literal = self.input[start..end],
        };
    }
    fn isAtEnd(self: *Self) bool {
        const curr_len = self.position - self.input.len;
        return curr_len >= self.input.len;
    }

    fn readIdentifier(self: *Self) token.Token {
        const p = self.position;
        while (isLetter(self.peekChar())) {
            self.readChar();
        }
        return self.makeToken(token.ident(self.input[p .. self.position + 1]), p, self.position + 1);
    }

    fn readString(self: *Self) token.Token {
        self.readChar();
        const p = self.position;
        while (true) {
            if ((self.ch == '"') or (self.ch == 0)) {
                break;
            }
            self.readChar();
        }
        return self.makeToken(token.Type.STRING, p, self.position);
    }

    fn readInt(self: *Self) token.Token {
        const p = self.position;
        while (isDigit(self.peekChar())) {
            self.readChar();
        }
        return self.makeToken(token.Type.INT, p, self.position + 1);
    }

    fn skipWhitespace(self: *Self) void {
        while (true) {
            switch (self.ch) {
                ' ', '\r', '\t' => _ = self.readChar(),
                '\n' => {
                    self.line += 1;
                    _ = self.readChar();
                },
                else => return,
            }
        }
    }
};

const mem = std.mem;
const expect = std.testing.expect;
const test_allocator = std.testing.allocator;

test "test simple tokens" {
    const input = "=+(){},;";
    const nextToken = struct {
        str: []const u8,
        tok: token.Type,
    };
    const tests = &[_]*const nextToken{
        &nextToken{ .str = "=", .tok = token.Type.ASSIGN },
        &nextToken{ .str = "+", .tok = token.Type.PLUS },
        &nextToken{ .str = "(", .tok = token.Type.LPAREN },
        &nextToken{ .str = ")", .tok = token.Type.RPAREN },
        &nextToken{ .str = "{", .tok = token.Type.LBRACE },
        &nextToken{ .str = "}", .tok = token.Type.RBRACE },
        &nextToken{ .str = ",", .tok = token.Type.COMMA },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },
        &nextToken{ .str = "", .tok = token.Type.EOF },
    };

    var lex: Lexer = Lexer.init(input);

    for (tests) |t| {
        const ntk = lex.nextToken();

        if (!mem.eql(u8, t.str, ntk.literal)) {
            std.debug.print("\nTest fail: str \"{s}\" token is \"{s}\"\n", .{ t.str, ntk.literal });
            try expect(false);
        }
        if (t.tok != ntk.token) {
            std.debug.print("\nTest fail: str \"{s}\" token is \"{s}\"\n", .{ @tagName(t.tok), @tagName(ntk.token) });
            try expect(false);
        }
    }
}

test "test next token" {
    const input =
        \\let five = 5;
        \\let ten = 10;
        \\
        \\let add = fn(x, y) {
        \\	x + y;
        \\};
        \\
        \\let result = add(five, ten);
        \\!-/*5;
        \\5 < 10 > 5;
        \\
        \\if (5 < 10) {
        \\	return true;
        \\} else {
        \\	return false;
        \\}
        \\
        \\10 == 10;
        \\10 != 9;
        \\
        \\"foobar"
        \\"foo bar"
        \\
        \\[1, 2];
        \\{"foo": "bar"};
    ;

    const nextToken = struct {
        str: []const u8,
        tok: token.Type,
    };
    const tests = &[_]*const nextToken{
        &nextToken{ .str = "let", .tok = token.Type.LET },
        &nextToken{ .str = "five", .tok = token.Type.IDENT },
        &nextToken{ .str = "=", .tok = token.Type.ASSIGN },
        &nextToken{ .str = "5", .tok = token.Type.INT },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },

        &nextToken{ .str = "let", .tok = token.Type.LET },
        &nextToken{ .str = "ten", .tok = token.Type.IDENT },
        &nextToken{ .str = "=", .tok = token.Type.ASSIGN },
        &nextToken{ .str = "10", .tok = token.Type.INT },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },

        &nextToken{ .str = "let", .tok = token.Type.LET },
        &nextToken{ .str = "add", .tok = token.Type.IDENT },
        &nextToken{ .str = "=", .tok = token.Type.ASSIGN },
        &nextToken{ .str = "fn", .tok = token.Type.FUNCTION },
        &nextToken{ .str = "(", .tok = token.Type.LPAREN },
        &nextToken{ .str = "x", .tok = token.Type.IDENT },
        &nextToken{ .str = ",", .tok = token.Type.COMMA },
        &nextToken{ .str = "y", .tok = token.Type.IDENT },
        &nextToken{ .str = ")", .tok = token.Type.RPAREN },
        &nextToken{ .str = "{", .tok = token.Type.LBRACE },
        &nextToken{ .str = "x", .tok = token.Type.IDENT },
        &nextToken{ .str = "+", .tok = token.Type.PLUS },
        &nextToken{ .str = "y", .tok = token.Type.IDENT },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },
        &nextToken{ .str = "}", .tok = token.Type.RBRACE },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },

        &nextToken{ .str = "let", .tok = token.Type.LET },
        &nextToken{ .str = "result", .tok = token.Type.IDENT },
        &nextToken{ .str = "=", .tok = token.Type.ASSIGN },
        &nextToken{ .str = "add", .tok = token.Type.IDENT },
        &nextToken{ .str = "(", .tok = token.Type.LPAREN },
        &nextToken{ .str = "five", .tok = token.Type.IDENT },
        &nextToken{ .str = ",", .tok = token.Type.COMMA },
        &nextToken{ .str = "ten", .tok = token.Type.IDENT },
        &nextToken{ .str = ")", .tok = token.Type.RPAREN },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },

        &nextToken{ .str = "!", .tok = token.Type.BANG },
        &nextToken{ .str = "-", .tok = token.Type.MINUS },
        &nextToken{ .str = "/", .tok = token.Type.SLASH },
        &nextToken{ .str = "*", .tok = token.Type.ASTERISK },
        &nextToken{ .str = "5", .tok = token.Type.INT },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },

        &nextToken{ .str = "5", .tok = token.Type.INT },
        &nextToken{ .str = "<", .tok = token.Type.LT },
        &nextToken{ .str = "10", .tok = token.Type.INT },
        &nextToken{ .str = ">", .tok = token.Type.GT },
        &nextToken{ .str = "5", .tok = token.Type.INT },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },

        &nextToken{ .str = "if", .tok = token.Type.IF },
        &nextToken{ .str = "(", .tok = token.Type.LPAREN },
        &nextToken{ .str = "5", .tok = token.Type.INT },
        &nextToken{ .str = "<", .tok = token.Type.LT },
        &nextToken{ .str = "10", .tok = token.Type.INT },
        &nextToken{ .str = ")", .tok = token.Type.RPAREN },
        &nextToken{ .str = "{", .tok = token.Type.LBRACE },
        &nextToken{ .str = "return", .tok = token.Type.RETURN },
        &nextToken{ .str = "true", .tok = token.Type.TRUE },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },
        &nextToken{ .str = "}", .tok = token.Type.RBRACE },
        &nextToken{ .str = "else", .tok = token.Type.ELSE },
        &nextToken{ .str = "{", .tok = token.Type.LBRACE },
        &nextToken{ .str = "return", .tok = token.Type.RETURN },
        &nextToken{ .str = "false", .tok = token.Type.FALSE },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },
        &nextToken{ .str = "}", .tok = token.Type.RBRACE },

        &nextToken{ .str = "10", .tok = token.Type.INT },
        &nextToken{ .str = "==", .tok = token.Type.EQ },
        &nextToken{ .str = "10", .tok = token.Type.INT },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },

        &nextToken{ .str = "10", .tok = token.Type.INT },
        &nextToken{ .str = "!=", .tok = token.Type.NOT_EQ },
        &nextToken{ .str = "9", .tok = token.Type.INT },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },

        &nextToken{ .str = "foobar", .tok = token.Type.STRING },

        &nextToken{ .str = "foo bar", .tok = token.Type.STRING },

        &nextToken{ .str = "[", .tok = token.Type.LBRACKET },
        &nextToken{ .str = "1", .tok = token.Type.INT },
        &nextToken{ .str = ",", .tok = token.Type.COMMA },
        &nextToken{ .str = "2", .tok = token.Type.INT },
        &nextToken{ .str = "]", .tok = token.Type.RBRACKET },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },

        &nextToken{ .str = "{", .tok = token.Type.LBRACE },
        &nextToken{ .str = "foo", .tok = token.Type.STRING },
        &nextToken{ .str = ":", .tok = token.Type.COLON },
        &nextToken{ .str = "bar", .tok = token.Type.STRING },
        &nextToken{ .str = "}", .tok = token.Type.RBRACE },
        &nextToken{ .str = ";", .tok = token.Type.SEMICOLON },
    };

    var lex: Lexer = Lexer.init(input);

    for (tests) |t| {
        const ntk = lex.nextToken();

        //std.debug.print("\nt.str \"{s}\" ntk.literal is \"{s}\"\n", .{ t.str, ntk.literal });
        //std.debug.print("\nt.tok \"{s}\" ntk.token is \"{s}\"\n", .{ @tagName(t.tok), @tagName(ntk.token) });
        if (!mem.eql(u8, t.str, ntk.literal)) {
            std.debug.print("\nTest fail: t.str \"{s}\" ntk.literal is \"{s}\"\n", .{ t.str, ntk.literal });
            try expect(false);
        }
        if (t.tok != ntk.token) {
            std.debug.print("\nTest fail: t.tok \"{s}\" ntk.token is \"{s}\"\n", .{ @tagName(t.tok), @tagName(ntk.token) });
            try expect(false);
        }
    }
}
