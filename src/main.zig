const std = @import("std");
const Allocator = std.mem.Allocator;

const lexer = @import("./lexer.zig");
const token = @import("./token.zig");
const parser = @import("./parser.zig");

const errout = std.io.getStdErr().writer();
const stdout = std.io.getStdOut().writer();
const stdin = std.io.getStdIn().reader();

pub fn main() anyerror!u8 {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var allocator = gpa.allocator();
    defer _ = gpa.deinit();

    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    switch (args.len) {
        1 => repl(allocator),
        else => {
            errout.print("Usage: monkey [path]\n", .{}) catch {};
            std.process.exit(64);
        },
    }
    return 0;
}

fn repl(allocator: Allocator) void {
    var buf = std.io.bufferedReader(stdin);
    var reader = buf.reader();
    var line_buf: [1024]u8 = undefined;

    while (true) {
        stdout.writeAll(">> ") catch std.debug.panic("Couldn't write to stdout you have serious problems", .{});
        var line = reader.readUntilDelimiterOrEof(&line_buf, '\n') catch {
            std.debug.panic("Couldn't read from stdin in repl you have serious problems", .{});
        } orelse {
            stdout.writeAll("\n") catch std.debug.panic("Couldn't write to stdout you have serious problems", .{});
            break;
        };

        var lex: lexer.Lexer = lexer.Lexer.init(line);
        while (true) {
            const tok: token.Token = lex.nextToken();
            if (tok.token == token.Type.EOF) {
                break;
            }
            std.debug.print("Token: Line {d} Type {s} and Literal {s}\n", .{ tok.line, @tagName(tok.token), tok.literal });
        }
        var psr: parser.Parser.init(allocator, &lex);
        if (psr.lex == undefined) {
            std.debug.print("Error lex is undefined in parser");
            break;
        }
        var statements = psr.parseProgram();
        if (statements) |stmts| {
        std.debug.print("Statements count is {d}\n", stmts.items.len);
        } else {
            std.debug.print("Not statements");
        }
    }
}
