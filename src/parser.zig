const std = @import("std");
const Lexer = @import("lexer.zig").Lexer;
const Token = @import("token.zig").Token;
const Type = @import("token.zig").Type;
const Statement = @import("ast.zig").Statement;
const Expression = @import("ast.zig").Expression;
const Node = @import("ast.zig").Node;
const ast = @import("ast.zig");

const OpOrder = enum(u8) {
    LOWEST = 0,
    EQUALS = 1,
    LESSGREATER = 2,
    SUM = 3,
    PRODUCT = 4,
    PREFIX = 5,
    CALL = 6,
    INDEX = 7,
};

const ParseError = error{
    Unknown,
    MissingParser,
    NotParsable,
    ExpectedIdentifier,
    ExpectedAssignment,
};

pub const Parser = struct {
    const Self = @This();
    lex: *Lexer,
    curTok: Token,
    peekTok: Token,
    allocator: std.mem.Allocator,
    pub fn init(allocator: std.mem.Allocator, lex: *Lexer) Parser {
        const ct = lex.nextToken();
        const pt = lex.nextToken();
        const p = Parser{
            .allocator = allocator,
            .lex = lex,
            .curTok = ct,
            .peekTok = pt,
        };
        return p;
    }

    pub fn nextToken(self: *Self) void {
        self.curTok = self.peekTok;
        self.peekTok = self.lex.nextToken();
    }

    pub fn parseLetStatement(self: *Self) !Node {
        const curTok = self.curTok;
        if (!self.expectPeek(Type.IDENT)) {
            return ParseError.ExpectedIdentifier;
        }
        const idtf = ast.Identifier.init(curTok, self.curTok.literal);

        if (!self.expectPeek(Type.ASSIGN)) {
            return ParseError.ExpectedAssignment;
        }
        self.nextToken();
        const parsedExp = self.parseExpression(OpOrder.LOWEST);
        defer {
            if (self.peekTokenIs(Type.SEMICOLON)) {
                self.nextToken();
            }
        }
        if (parsedExp) |exp| {
            const stmt = ast.LetStatement.init(curTok, idtf);
            self.nextToken();
            std.debug.print("Expression is {}\n", .{exp.string(self.allocator, exp)});
            var children = std.ArrayList(Node).init(self.allocator);
            try children.append(exp);
            return Node{ .statement = Statement{ .let = stmt }, .children = children };
        } else {
            return ParseError.Unknown;
        }
    }

    fn parseReturnStatement(self: *Self) !Node {
        const returnTok = self.curTok;
        self.nextToken();
        const parsedExp = self.parseExpression(OpOrder.LOWEST);
        defer {
            if (self.peekTokenIs(Type.SEMICOLON)) {
                self.nextToken();
            }
        }
        if (parsedExp) |exp| {
            const stmt = ast.ReturnStatement.init(self.allocator, returnTok, exp.expression);
            self.nextToken();
            if (stmt.value) |foo| {
                std.debug.print("Expression is {}\n", .{foo});
            }
            return Node{ .statement = Statement{ .returnStmt = stmt } };
        }
        return ParseError.Unknown;
    }

    pub fn parseStatement(self: *Self) !Node {
        std.debug.print("parseStatement {} .....\n", .{self.curTok.token});
        return switch (self.curTok.token) {
            Type.LET => try self.parseLetStatement(),
            Type.RETURN => self.parseReturnStatement(),
            else => self.parseExpressionStatement(),
        };
    }

    fn parseExpressionStatement(self: *Self) !Node {
        const parsedExp = self.parseExpression(OpOrder.LOWEST);
        defer {
            if (self.peekTokenIs(Type.SEMICOLON)) {
                self.nextToken();
            }
        }
        if (parsedExp) |exp| {
            const stmt = ast.ExpressionStatement.init(self.allocator, self.curTok, exp.expression);
            if (stmt.value) |foo| {
                std.debug.print("Statement Expression is {}\n", .{foo});
            }
            return Node{ .statement = Statement{ .expression = stmt } };
        }
        return ParseError.Unknown;
    }

    fn parseIdentifier(self: *Self) ?Node {
        return Node{ .ident = ast.Identifier{ .token = self.curTok, .value = self.curTok.literal } };
    }

    fn parseIntegerLiteral(self: *Self) ?Node {
        const val = std.fmt.parseInt(i64, self.curTok.literal, 10) catch |err| {
            std.debug.print("Error parsing integer {}....\n", .{err});
            return null;
        };
        return Node{ .int = ast.Integer{ .token = self.curTok, .value = val } };
    }

    fn parseBoolean(self: *Self) ?Node {
        std.debug.print("Calling parseBoolean {}\n", .{self.curTokenIs(Type.TRUE)});
        return Node{ .boolean = ast.Boolean{ .token = self.curTok, .value = self.curTokenIs(Type.TRUE) } };
    }

    fn parseString(self: *Self) ?Node {
        std.debug.print("Calling parseString '{s}'\n", .{self.curTok.literal});
        return Node{ .string = ast.String{ .token = self.curTok, .value = self.curTok.literal } };
    }

    fn prefix(self: *Self) ?Node {
        const exp = ast.PrefixExpression.init(self.curTok, self.curTok.literal);
        self.nextToken();
        const right = self.parseExpression();
        if (right) |r| {
            switch (r) {
                Node.identifier => exp.ident = r.identifier,
                Node.integer => exp.int = r.integer,
                Node.boolean => exp.boolean = r.boolean,
                Node.string => exp.string = r.string,
                Node.prefixExpression => exp.prefix = r,
                Node.infixExpression => exp.infix = r,
            }
        }
        return Node{ .prefixExpression = exp };
    }
    fn parsePrefixExpression(self: *Self) ?Node {
        return switch (self.curTok.token) {
            Type.IDENT => self.parseIdentifier(),
            Type.INT => self.parseIntegerLiteral(),
            Type.BANG => self.prefix(),
            Type.MINUS => self.prefix(),
            Type.PLUS => self.prefix(),
            Type.TRUE => self.parseBoolean(),
            Type.FALSE => self.parseBoolean(),
            Type.STRING => self.parseString(),
            // Type.LPAREN => self.parseGroupedExpression(),
            // Type.IF => self.parseIfExpression(),
            // Type.FUNCTION => self.parseFunctionLiteral(),
            // Type.LBRACKET => self.parseArrayLiteral(),
            // Type.LBRACE => self.parseHashLiteral(),
            else => null,
        };
    }

    fn parseInfixExpression(self: *Self, left: Node) ?Node {
        const leftExp = left.expression;
        const tok = self.curTok;
        const precedence = self.curPrecedence();
        self.nextToken();
        const right = self.parseExpression(precedence);
        if (right) |r| {
            const exp = ast.InfixExpression{
                .token = tok,
                .left = leftExp,
                .operator = tok.literal,
                .right = r.expression,
            };
            return Node{ .infixExpression = exp };
        }
        return null;
    }

    fn parseInfix(self: *Self, left: Node) ?Node {
        return switch (self.curTok.token) {
            Type.PLUS => self.parseInfixExpression(left),
            Type.MINUS => self.parseInfixExpression(left),
            Type.SLASH => self.parseInfixExpression(left),
            Type.ASTERISK => self.parseInfixExpression(left),
            Type.EQ => self.parseInfixExpression(left),
            Type.NOT_EQ => self.parseInfixExpression(left),
            Type.LT => self.parseInfixExpression(left),
            Type.GT => self.parseInfixExpression(left),
            //Type.LPAREN => self.parseCallExpression(left),
            Type.LBRACKET => self.parseInfixExpression(left),
            else => null,
        };
    }

    fn parseExpression(self: *Self, precedence: OpOrder) ?Node {
        const leftNode = self.parsePrefixExpression();
        std.debug.print("parseExpression precedence is {}, token {} and leftNode {?} \n", .{ precedence, self.curTok.token, leftNode });

        if (leftNode) |n| {
            while (!self.peekTokenIs(Type.SEMICOLON) and @intFromEnum(precedence) < @intFromEnum(self.peekPrecedence())) {
                const infix = self.parseInfix(n);
                if (infix == null) {
                    return leftNode;
                }
                self.nextToken();
            }
        }
        return leftNode;
    }

    fn expectPeek(self: *Self, t: Type) bool {
        if (self.peekTokenIs(t)) {
            self.nextToken();
            return true;
        } else {
            return false;
        }
    }

    fn peekError(self: *Self, t: Type) void {
        self.errors.append(t.literal);
    }

    fn peekTokenIs(self: *Self, t: Type) bool {
        return self.peekTok.token == t;
    }

    fn curPrecedence(self: *Self) OpOrder {
        return switch (self.curTok.token) {
            Type.EQ => OpOrder.EQUALS,
            Type.NOT_EQ => OpOrder.EQUALS,
            Type.LT => OpOrder.LESSGREATER,
            Type.GT => OpOrder.LESSGREATER,
            Type.PLUS => OpOrder.SUM,
            Type.MINUS => OpOrder.SUM,
            Type.SLASH => OpOrder.PRODUCT,
            Type.ASTERISK => OpOrder.PRODUCT,
            Type.LPAREN => OpOrder.CALL,
            Type.LBRACKET => OpOrder.INDEX,
            else => OpOrder.LOWEST,
        };
    }

    fn peekPrecedence(self: *Self) OpOrder {
        return switch (self.peekTok.token) {
            Type.EQ => OpOrder.EQUALS,
            Type.NOT_EQ => OpOrder.EQUALS,
            Type.LT => OpOrder.LESSGREATER,
            Type.GT => OpOrder.LESSGREATER,
            Type.PLUS => OpOrder.SUM,
            Type.MINUS => OpOrder.SUM,
            Type.SLASH => OpOrder.PRODUCT,
            Type.ASTERISK => OpOrder.PRODUCT,
            Type.LPAREN => OpOrder.CALL,
            Type.LBRACKET => OpOrder.INDEX,
            else => OpOrder.LOWEST,
        };
    }

    fn curTokenIs(self: *Self, t: Type) bool {
        return self.curTok.token == t;
    }

    pub fn parseProgram(self: *Self) !std.ArrayList(Statement) {
        var statements = std.ArrayList(Statement).init(self.allocator);

        std.debug.print("\nCurrent token is {}.....\n", .{self.curTok.token});
        while (self.curTok.token != Type.EOF) {
            const n = try self.parseStatement();
            std.debug.print("Statement {}\n", .{n.statement});

            try statements.append(n.statement);
            self.nextToken();
        }
        return statements;
    }
};

const test_allocator = std.testing.allocator;

const expect = std.testing.expect;

test "test let statement" {
    const ValueTypeTag = enum {
        int,
        boolean,
        string,
    };
    const ValueType = union(ValueTypeTag) {
        int: u32,
        boolean: bool,
        string: []const u8,
    };

    const stmtTest = struct {
        input: []const u8,
        idntf: []const u8,
        expVal: ValueType,
    };
    const tests = &[_]*const stmtTest{
        &stmtTest{ .input = "let x = 5;", .idntf = "x", .expVal = ValueType{ .int = 5 } },
        &stmtTest{ .input = "let y = true;", .idntf = "y", .expVal = ValueType{ .boolean = true } },
        &stmtTest{ .input = "let z = false;", .idntf = "z", .expVal = ValueType{ .boolean = false } },
        &stmtTest{ .input = "let foobar = \"why\";", .idntf = "foobar", .expVal = ValueType{ .string = "why" } },
    };
    for (tests) |t| {
        var lex: Lexer = Lexer.init(t.input);
        var psr: Parser = Parser.init(test_allocator, &lex);
        if (psr.lex == undefined) {
            try expect(false);
        }
        const statements = psr.parseProgram();
        if (statements) |stmts| {
            defer stmts.deinit();
            std.debug.print("input '{s}' Size of statements is {d}\n", .{ t.input, stmts.items.len });
            try std.testing.expect(@TypeOf(stmts) == std.ArrayList(Statement));
            const prog = ast.Program.init(stmts);
            try std.testing.expect(@TypeOf(prog) == ast.Program);
            try std.testing.expect(prog.statements.items.len == 1);
            const stmt = prog.statements.items[0];
            try expect(std.mem.eql(u8, stmt.let.name.value, t.idntf));

            switch (t.expVal) {
                ValueType.int => {
                    if (stmt.let.value) |exp| {
                        try expect(exp.int.value == t.expVal.int);
                    } else {
                        try expect(false);
                    }
                },
                ValueType.boolean => {
                    if (stmt.let.value) |exp| {
                        try expect(exp.boolean.value == t.expVal.boolean);
                    } else {
                        try expect(false);
                    }
                },
                ValueType.string => {
                    if (stmt.let.value) |exp| {
                        try expect(std.mem.eql(u8, exp.string.value, t.expVal.string));
                    } else {
                        try expect(false);
                    }
                },
            }
        } else |e| {
            std.debug.print("Error parsing program {}\n", .{e});
            if (@errorReturnTrace()) |trace| {
                std.debug.dumpStackTrace(trace.*);
            }
            try expect(false);
        }
    }
}

test "test return statement" {
    const input =
        \\
        \\return 5;
        \\return 10;
        \\return 993322;
    ;
    var lex: Lexer = Lexer.init(input);
    var psr: Parser = Parser.init(test_allocator, &lex);
    if (psr.lex == undefined) {
        try expect(false);
    }
    var stmts = psr.parseProgram() catch |err| {
        std.debug.print("Error {}\n", .{err});
        try expect(false);
        return;
    };
    defer stmts.deinit();
    try expect(stmts.items.len == 3);
    var i: usize = 0;
    while (i < stmts.items.len) : (i += 1) {
        const stm = stmts.items[i];
        if (!std.mem.eql(u8, stm.returnStmt.token.literal, "return")) {
            std.debug.print("Expected 'return' bug got '{s}'\n", .{stm.returnStmt.token.literal});
            try expect(false);
            return;
        }
    }
}

test "test integer literal expression" {
    const input = "5;";

    var lex: Lexer = Lexer.init(input);
    var psr: Parser = Parser.init(test_allocator, &lex);
    if (psr.lex == undefined) {
        try expect(false);
    }
    var stmts = psr.parseProgram() catch |err| {
        std.debug.print("Error {}\n", .{err});
        try expect(false);
        return;
    };
    defer stmts.deinit();
    try expect(stmts.items.len == 1);
    const stm = stmts.items[0];
    if (stm.expression.value) |exp| {
        try expect(exp.int.value == 5);
    } else {
        try expect(false);
    }
}

test "test identifier expression" {
    const input = "foobar;";

    var lex: Lexer = Lexer.init(input);
    var psr: Parser = Parser.init(test_allocator, &lex);
    if (psr.lex == undefined) {
        try expect(false);
    }
    var stmts = psr.parseProgram() catch |err| {
        std.debug.print("Error {}\n", .{err});
        try expect(false);
        return;
    };
    defer stmts.deinit();
    try expect(stmts.items.len == 1);
    const stm = stmts.items[0];
    if (stm.expression.value) |exp| {
        std.debug.print("Identity is '{s}'\n", .{exp.ident.value});
        try expect(!std.mem.eql(u8, exp.ident.value, input));
    } else {
        try expect(false);
    }
}

// test "test prefix parser" {
//     const ValueTypeTag = enum {
//         int,
//         boolean,
//     };
//     const ValueType = union(ValueTypeTag) {
//         int: u32,
//         boolean: bool,
//     };
//
//     const stmtTest = struct {
//         input: []const u8,
//         operator: []const u8,
//         expVal: ValueType,
//     };
//     var tests = &[_]*const stmtTest{
//         &stmtTest{ .input = "!5;", .operator = "!", .expVal = ValueType{ .int = 5 } },
//         &stmtTest{ .input = "-15;", .operator = "-", .expVal = ValueType{ .int = 15 } },
//         &stmtTest{ .input = "!true;", .operator = "!", .expVal = ValueType{ .boolean = false } },
//         &stmtTest{ .input = "!false;", .operator = "!", .expVal = ValueType{ .boolean = false } },
//            try std.testing.expect(prog.statements.items.len == 1);
//            var stmt = prog.statements.items[0];
//            try expect(std.mem.eql(u8, stmt.expression.token.literal, t.operator));
//
//            switch (t.expVal) {
//                ValueType.int => {
//                    if (stmt.let.value) |exp| {
//                        try expect(exp.int.value == t.expVal.int);
//                    } else {
//                        try expect(false);
//                    }
//                },
//                ValueType.boolean => {
//                    if (stmt.let.value) |exp| {
//                        try expect(exp.boolean.value == t.expVal.boolean);
//                    } else {
//                        try expect(false);
//                    }
//                },
test "test parsing infix expressions" {
    const ValueTypeTag = enum {
        int,
        boolean,
        string,
    };
    const ValueType = union(ValueTypeTag) {
        const Self = @This();
        int: u32,
        boolean: bool,
        string: []const u8,
        fn expectValue(self: *Self, stmt: ast.Statement) void {
            switch (self) {
                ValueTypeTag.int => {
                    if (stmt.expression.value) |exp| {
                        try expect(exp.int.value == self.int);
                    } else {
                        try expect(false);
                    }
                },
                ValueTypeTag.boolean => {
                    if (stmt.expression.value) |exp| {
                        try expect(exp.boolean.value == self.boolean);
                    } else {
                        try expect(false);
                    }
                },
                ValueTypeTag.string => {
                    if (stmt.expression.value) |exp| {
                        try expect(std.mem.eql(u8, exp.string.value, self.string));
                    } else {
                        try expect(false);
                    }
                },
            }
        }
    };
    const Resp = struct {
        left: ValueType,
        operator: []const u8,
        right: ValueType,
    };
    const stmtTest = struct {
        input: []const u8,
        expVal: Resp,
    };

    const tests = &[_]*const stmtTest{
        &stmtTest{ .input = "5 + 5;", .expVal = Resp{ .left = ValueType{ .int = 5 }, .operator = "+", .right = ValueType{ .int = 5 } } },
    };
    for (tests) |t| {
        var lex: Lexer = Lexer.init(t.input);
        var psr: Parser = Parser.init(test_allocator, &lex);
        if (psr.lex == undefined) {
            try expect(false);
        }
        const statements = psr.parseProgram();
        if (statements) |stmts| {
            defer stmts.deinit();
            std.debug.print("input '{s}' Size of statements is {d}\n", .{ t.input, stmts.items.len });
            try std.testing.expect(@TypeOf(stmts) == std.ArrayList(Statement));
            const prog = ast.Program.init(stmts);
            try std.testing.expect(@TypeOf(prog) == ast.Program);
            try std.testing.expect(prog.statements.items.len == 1);
            const leftStmt = prog.statements.items[0];
            if (leftStmt.expression.value) |exp| {
                try expect(exp.int.value == t.expVal.left.int);
            } else {
                try expect(false);
            }
            const op = prog.statements.items[1];
            if (op.expression.value) |exp| {
                try expect(std.mem.eql(u8, exp.string.value, t.expVal.operator));
            } else {
                try expect(false);
            }
            const rightStmt = prog.statements.items[2];
            if (rightStmt.expression.value) |exp| {
                try expect(exp.int.value == t.expVal.right.int);
            } else {
                try expect(false);
            }
        } else |e| {
            std.debug.print("Error parsing program {}\n", .{e});
            if (@errorReturnTrace()) |trace| {
                std.debug.dumpStackTrace(trace.*);
            }
            try expect(false);
        }
    }
}
