const std = @import("std");
const mem = std.mem;

pub const Type = enum {
    ILLEGAL,
    EOF,
    ERROR,
    // Identifiers and literals
    IDENT,
    INT,
    // String
    STRING,
    // Operators
    ASSIGN,
    PLUS,
    MINUS,
    ASTERISK,
    SLASH,
    BANG,

    LT,
    GT,
    EQ,
    NOT_EQ,
    // Commments
    COMMENT,

    // Delimiters
    COMMA,
    SEMICOLON,
    COLON,

    LPAREN,
    RPAREN,
    LBRACE,
    RBRACE,
    LBRACKET,
    RBRACKET,

    // Keyword
    FUNCTION,
    LET,
    TRUE,
    FALSE,
    IF,
    ELSE,
    RETURN,
};

pub fn ident(idt: []const u8) Type {
    if (mem.eql(u8, idt, "fn")) {
        return Type.FUNCTION;
    } else if (mem.eql(u8, idt, "let")) {
        return Type.LET;
    } else if (mem.eql(u8, idt, "true")) {
        return Type.TRUE;
    } else if (mem.eql(u8, idt, "false")) {
        return Type.FALSE;
    } else if (mem.eql(u8, idt, "if")) {
        return Type.IF;
    } else if (mem.eql(u8, idt, "else")) {
        return Type.ELSE;
    } else if (mem.eql(u8, idt, "return")) {
        return Type.RETURN;
    }
    return Type.IDENT;
}

pub fn lookup(tok: Type) []const u8 {
    return switch (tok) {
        Type.ILLEGAL => "ILLEGAL",
        Type.EOF => "EOF",
        Type.ERROR => "ERROR",
        Type.IDENT => "IDENT",
        Type.STRING => "STRING",
        Type.INT => "INT",
        Type.ASSIGN => "=",
        Type.PLUS => "+",
        Type.MINUS => "-",
        Type.ASTERISK => "*",
        Type.SLASH => "/",
        Type.BANG => "!",
        Type.LT => "<",
        Type.GT => ">",
        Type.EQ => "==",
        Type.NOT_EQ => "!=",
        Type.COMMENT => "//",
        Type.COMMA => ",",
        Type.SEMICOLON => ";",
        Type.COLON => ":",
        Type.LPAREN => "(",
        Type.RPAREN => ")",
        Type.LBRACE => "{",
        Type.RBRACE => "}",
        Type.LBRACKET => "[",
        Type.RBRACKET => "]",
        Type.FUNCTION => "FUNCTION",
        Type.LET => "LET",
        Type.TRUE => "TRUE",
        Type.FALSE => "FALSE",
        Type.IF => "IF",
        Type.ELSE => "ELSE",
        Type.RETURN => "RETURN",
    };
}

pub const Token = struct {
    token: Type,
    literal: []const u8,
    line: i32,
};

const expect = std.testing.expect;
const test_allocator = std.testing.allocator;

test "mapping of tokenen to string" {
    try expect(mem.eql(u8, lookup(Type.ILLEGAL), "ILLEGAL"));
    try expect(mem.eql(u8, lookup(Type.EOF), "EOF"));
    try expect(mem.eql(u8, lookup(Type.IDENT), "IDENT"));
    try expect(mem.eql(u8, lookup(Type.INT), "INT"));
    try expect(mem.eql(u8, lookup(Type.ASSIGN), "="));
    try expect(mem.eql(u8, lookup(Type.PLUS), "+"));
    try expect(mem.eql(u8, lookup(Type.MINUS), "-"));
}

test "Ident lookup returns right token" {
    try std.testing.expectEqual(Type.FUNCTION, ident("fn"));
    try std.testing.expectEqual(Type.LET, ident("let"));
    try std.testing.expectEqual(Type.TRUE, ident("true"));
    try std.testing.expectEqual(Type.FALSE, ident("false"));
    try std.testing.expectEqual(Type.IF, ident("if"));
    try std.testing.expectEqual(Type.ELSE, ident("else"));
    try std.testing.expectEqual(Type.RETURN, ident("return"));
    try std.testing.expectEqual(Type.IDENT, ident("foo"));
}

test "token Lookup corrent string" {
    const LookupTest = struct {
        str: []const u8,
        tok: Type,
    };
    const tests = &[_]*const LookupTest{
        &LookupTest{ .str = "ILLEGAL", .tok = Type.ILLEGAL },
        &LookupTest{ .str = "EOF", .tok = Type.EOF },
        &LookupTest{ .str = "ERROR", .tok = Type.ERROR },
        &LookupTest{ .str = "IDENT", .tok = Type.IDENT },
        &LookupTest{ .str = "STRING", .tok = Type.STRING },
        &LookupTest{ .str = "=", .tok = Type.ASSIGN },
        &LookupTest{ .str = "+", .tok = Type.PLUS },
        &LookupTest{ .str = "-", .tok = Type.MINUS },
        &LookupTest{ .str = "*", .tok = Type.ASTERISK },
        &LookupTest{ .str = "/", .tok = Type.SLASH },
        &LookupTest{ .str = "<", .tok = Type.LT },
        &LookupTest{ .str = ">", .tok = Type.GT },
        &LookupTest{ .str = "==", .tok = Type.EQ },
        &LookupTest{ .str = "!=", .tok = Type.NOT_EQ },
        &LookupTest{ .str = "//", .tok = Type.COMMENT },
        &LookupTest{ .str = ",", .tok = Type.COMMA },
        &LookupTest{ .str = ";", .tok = Type.SEMICOLON },
        &LookupTest{ .str = ":", .tok = Type.COLON },
        &LookupTest{ .str = "(", .tok = Type.LPAREN },
        &LookupTest{ .str = ")", .tok = Type.RPAREN },
        &LookupTest{ .str = "{", .tok = Type.LBRACE },
        &LookupTest{ .str = "}", .tok = Type.RBRACE },
        &LookupTest{ .str = "[", .tok = Type.LBRACKET },
        &LookupTest{ .str = "]", .tok = Type.RBRACKET },
        &LookupTest{ .str = "FUNCTION", .tok = Type.FUNCTION },
        &LookupTest{ .str = "LET", .tok = Type.LET },
        &LookupTest{ .str = "TRUE", .tok = Type.TRUE },
        &LookupTest{ .str = "FALSE", .tok = Type.FALSE },
        &LookupTest{ .str = "IF", .tok = Type.IF },
        &LookupTest{ .str = "ELSE", .tok = Type.ELSE },
        &LookupTest{ .str = "RETURN", .tok = Type.RETURN },
    };

    for (tests) |t| {
        if (!mem.eql(u8, t.str, lookup(t.tok))) {
            std.debug.print("\nTest fail: str \"{s}\" token is \"{s}\"\n", .{ t.str, @tagName(t.tok) });
            try expect(false);
        }
    }
}
